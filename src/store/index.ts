import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from '../reducers';
import { StoreState } from '../types';

const loggerMiddleware = createLogger();

export default function configureStore(
  initialState: StoreState = {
    vacancies: {
      isLoading: false,
      items: {},
      currentPage: 1,
      totalPages: 1,
      loadedIds: new Set()
    }
  }
) {
  return createStore(
    rootReducer,
    initialState!,
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware
    )
  );
}
