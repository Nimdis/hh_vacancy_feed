import * as fetch from 'isomorphic-fetch';
import { Dispatch } from 'react-redux';
import * as constants from '../constants';
import { VacancyItems, Vacancy } from '../types';
import config from '../config';

export interface RequestVacancies {
  type: constants.REQUEST_VACANCIES;
  isLoading: boolean;
}

export function requestVacancies(): RequestVacancies {
  return {
    type: constants.REQUEST_VACANCIES,
    isLoading: true
  };
}

export interface FetchVacancies {
  type: constants.FETCH_VACANCIES;
  isLoading: boolean;
  items: VacancyItems;
  currentPage: number;
  totalPages: number;
  loadedIds: Set<string>;
}

export function receiveVacancies(
  receivedItems: Array<Vacancy>,
  currentPage: number,
  totalPages: number
): FetchVacancies {
  const loadedIds = new Set();
  const items = {};
  for (const item of receivedItems) {
    loadedIds.add(item.id);
    items[item.id] = item;
  }
  return {
    type: constants.FETCH_VACANCIES,
    isLoading: false,
    items,
    loadedIds,
    currentPage,
    totalPages
  };
}

export function fetchVacancies(
  dispatch: Dispatch<RequestVacancies | FetchVacancies>,
  currentPage: number
) {
  dispatch(requestVacancies());
  return fetch(
    `${config.apiUrl}/vacancies?order_by=publication_time&&page=${currentPage}`
  )
    .then(response => response.json())
    .then(json => dispatch(receiveVacancies(json.items, json.page, json.pages)))
  // NOTICE should be refactored
    .catch(err => console.error(err));
}
