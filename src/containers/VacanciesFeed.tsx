import { StoreState } from '../types';
import { connect, Dispatch } from 'react-redux';
import { fetchVacancies, RequestVacancies, FetchVacancies } from '../actions';
import VacanciesFeed from '../components/VacanciesFeed';

function mapStateToProps(state: StoreState) {
  const {
    items, isLoading, currentPage, totalPages, loadedIds
  } = state.vacancies;
  return { items, isLoading, currentPage, totalPages, loadedIds };
}

function mapDispatchToProps(
  dispatch: Dispatch<RequestVacancies | FetchVacancies>
) {
  return {
    loadMore: (currentPage: number = 1) => {
      fetchVacancies(dispatch, currentPage);
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(VacanciesFeed);
