import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import registerServiceWorker from './registerServiceWorker';
import configureStore from './store';
import VacanciesFeedContainer from './containers/VacanciesFeed';

ReactDOM.render(
  <Provider store={configureStore()}>
    <VacanciesFeedContainer />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
