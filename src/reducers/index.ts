import { combineReducers } from 'redux';
import { REQUEST_VACANCIES, FETCH_VACANCIES } from '../constants';
import { VacanciesStoreState } from '../types';
import { RequestVacancies, FetchVacancies } from '../actions';

function vacancies(
  state: VacanciesStoreState = {
    isLoading: false,
    loadedIds: new Set(),
    items: {},
    currentPage: 1,
    totalPages: 1
  },
  action: RequestVacancies | FetchVacancies
): VacanciesStoreState {
  switch (action.type) {
    case REQUEST_VACANCIES:
      return {
        ...state,
        isLoading: action.isLoading
      };
    case FETCH_VACANCIES:
      return {
        isLoading: action.isLoading,
        items: Object.assign({}, state.items, action.items),
        loadedIds: new Set([
          ...Array.from(state.loadedIds),
          ...Array.from(action.loadedIds)
        ]),
        currentPage: action.currentPage,
        totalPages: action.totalPages
      };
    default:
      return state;
  }
}

export default combineReducers({ vacancies });
