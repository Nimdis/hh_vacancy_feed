import * as React from 'react';
import {
  InfinitePaginationProps,
  withInfinitePagination
} from './withInfinitePagination';
import { mount } from 'enzyme';
import * as sinon from 'sinon';

describe('withInfinitePagination', () => {
  interface Props {}

  class MockList extends React.Component<Props & InfinitePaginationProps> {
    render() {
      return <div />;
    }
  }

  let PaginatedList = withInfinitePagination(MockList);
  const loadMore = sinon.spy();

  beforeEach(() => {
    PaginatedList = withInfinitePagination(MockList);
  });

  it('should call componentDidMount', () => {
    sinon.spy(PaginatedList.prototype, 'componentDidMount');
    mount(
      <PaginatedList
        isLoading={false}
        currentPage={1}
        totalPages={10}
        loadMore={loadMore}
      />
    );
    expect(PaginatedList.prototype.componentDidMount).toHaveProperty('callCount', 1);
  });

  it('should scroll and load more', () => {
    global.window = {
      innerHeight: 1000,
      scrollY: 1000
    }
    const paginatedList = mount(
      <PaginatedList
        isLoading={false}
        currentPage={1}
        totalPages={10}
        loadMore={loadMore}
      />
    );
    paginatedList.instance().onScroll();
    expect(loadMore).toHaveProperty('callCount', 1);
    expect(loadMore.getCall(0).args[0]).toBe(2);
  });
});
