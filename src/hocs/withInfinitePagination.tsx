import * as React from 'react';

interface ExternalProps {
  isLoading: boolean;
  currentPage: number;
  totalPages: number;
  loadMore: (currentPage: number) => void;
}

export interface InfinitePaginationProps {
  isLoading: boolean;
  currentPage: number;
  totalPages: number;
  loadMore: (currentPage: number) => void;
}

export const withInfinitePagination = <OriginalProps extends {}>(
    Component: (React.ComponentClass<OriginalProps & InfinitePaginationProps>
      | React.StatelessComponent<OriginalProps & InfinitePaginationProps>)
  ) => {
    type ResultProps = OriginalProps & ExternalProps;
    const result = class WithInfinitePagination extends React.Component<ResultProps> {
      constructor(props: ResultProps) {
        super(props);
        this.onScroll = this.onScroll.bind(this);
      }

      componentDidMount() {
        window.addEventListener('scroll', this.onScroll, false);
      }

      componentWillUnmount() {
        window.removeEventListener('scroll', this.onScroll, false);
      }

      protected onScroll() {
        if (
          (window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 500) &&
          !this.props.isLoading &&
          this.props.currentPage < this.props.totalPages
        ) {
          this.props.loadMore(this.props.currentPage + 1);
        }
      }

      render() {
        return <Component {...this.props} />;
      }
    };

    return result;
  };
