export interface Vacancy {
  id: string;
  type: {
    id: string;
    name: string;
  };
  area: {
    id: string;
    name: string;
  };
  name: string;
  snippet: {
    requirement: string;
    responsibility: string;
  };
  employer?: {
    name: string;
  };
  published_at: Date;
}

export interface VacancyItems {
  [index: string]: Vacancy;
}

export interface VacanciesStoreState {
  isLoading: boolean;
  items: VacancyItems;
  currentPage: number;
  totalPages: number;
  loadedIds: Set<string>;
}

export interface StoreState {
  vacancies: VacanciesStoreState;
}
