import './VacancyCard.sass';
import * as React from 'react';
import * as moment from 'moment';
import 'moment/locale/ru';
import { Vacancy } from '../types';

moment.locale('ru');

interface Props {
  item: Vacancy;
}

export default class VacancyCard extends React.Component<Props> {
  render() {
    const item = this.props.item;
    return (
      <div className="vacancy-card">
        <div className="vacancy-name">{item.name}</div>
        <div className="vacancy-area">{item.area.name}</div>
        {item.employer &&
          <div className="vacancy-employer-name">{item.employer.name}</div>
        }
        <div className="vacancy-requirement">{item.snippet.requirement}</div>
        <div className="vacancy-responsibility">{item.snippet.responsibility}</div>
        <div className="vacancy-published-at">{moment(item.published_at).fromNow()}</div>
      </div>
    );
  }
}
