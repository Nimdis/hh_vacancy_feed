import './VacanciesFeed.sass';
import * as React from 'react';
import { VacancyItems } from '../types';
import VacancyCard from './VacancyCard';
import {
  InfinitePaginationProps,
  withInfinitePagination
} from '../hocs/withInfinitePagination';

interface Props {
  items: VacancyItems;
  loadedIds: Set<string>;
  isLoading: boolean;
  loadMore: () => void;
}

class VacanciesFeed extends React.Component<Props & InfinitePaginationProps> {
  componentDidMount() {
    this.props.loadMore();
  }

  protected renderItems() {
    return Array.from(this.props.loadedIds).map(id => (
      <VacancyCard item={this.props.items[id]} key={id} />
    ));
  }

  protected renderLoading() {
    if (!this.props.isLoading) {
      return null;
    }
    return <div>Loading...</div>;
  }

  render() {
    return (
      <div className="vacancy-feed">
        {this.renderItems()}
        {this.renderLoading()}
      </div>
    );
  }
}

export default withInfinitePagination(VacanciesFeed);
